
import java.sql.Connection;
import java.sql.DriverManager;


public class Main {

    /**
    * @param args the command line arguments
    */
    public static void main(String[] args) throws Exception {
        Connection conn = null;
        try {
            String driver = "com.mysql.cj.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/";
            String username = "root";
            String password = "Hdz12022003";//mat khau cua ban
            String dbName = "test";//ten csdl ma ban can ket noi
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + dbName, username, password);
            System.out.println("Kết nối thành công");
        } catch (Exception ex) {
            System.err.println("Không kết nối được với csdl");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                    System.out.println("Đóng kết nối");
                } catch (Exception ex) {
                }
            }
        }
    }
}